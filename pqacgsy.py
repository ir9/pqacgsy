# =*= coding:utf-8 =*=
# かしら
"""
ぷよぷよクエストアーケード カードゲットするやつ
"""

__author__  = "いろきゅう"
__version__ = "0.5.0"



import os
import os.path
import re
import time
import random
import base64
import traceback
import selenium
import selenium.webdriver
from selenium.webdriver            import support
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support    import expected_conditions as EC

# --- config ---
USERID   = ''
PASSWORD = ''
DLPath   = './img'

def launchBrowser() :
	return selenium.webdriver.Chrome()
	# return selenium.webdriver.Ie()
	# return selenium.webdriver.Edge()
# --- end of config ---

IMAGE_URL_EXTRACTOR = re.compile(r'/puyo/img/.+?\.jpg')
TOP_PAGE = 'https://puyoquest-am.net/puyo/login.html'
JS_IMAGE_DOWNLOADER = """
b64Img = null;	// global scope
(function(url){
	b64Img  = null;
	var xhr = new XMLHttpRequest();
	xhr.onload = function() {
		var reader = new FileReader();
		reader.onload = function() {
			// callback(reader.result);
			b64Img = reader.result;
		}
		reader.readAsDataURL(xhr.response);
	}
	xhr.open('get', url);
	xhr.responseType = 'blob';
	xhr.send();
})(arguments[0]);
"""

def _mkdir() :
	try :
		os.mkdir(DLPath)
	except FileExistsError as ex:
		pass	# ok

def _wait() :
	t = (random.random() * 3) + 3 # [3, 6) seconds
	time.sleep(t)

def _downloadImage(br, url, localFileName) :
	def download() :
		br.execute_script(JS_IMAGE_DOWNLOADER, url)
		time.sleep(0.2)
		
		printed = False
		for _ in range(30) :
			dataURLImg = br.execute_script("return b64Img;")
			if dataURLImg :
				if printed :
					print("")
				return dataURLImg
			printed = True
			print('.', end='')
			time.sleep(1)
		raise Exception("画像のダウンロードができなくてばたんきゅ～")

	def save(dataURLImg, localPath) :
		comma    = dataURLImg.index(",")
		b64Img   = dataURLImg[comma + 1:]
		rawImage = base64.b64decode(b64Img)
		try :
			_mkdir()
			with open(localPath, "wb") as hFile :
				hFile.write(rawImage)
		except FileExistsError as ex :
			pass	# OK

	localPath = os.path.join(DLPath, localFileName)
	if os.path.exists(localPath) :
		print("ファイルが存在するので飛ばします : " + localPath)
		return	# ファイル存在したらDLせずに帰る

	dataURLImage = download()
	save(dataURLImage, localPath)

def _downloadImageFromDetailPage(br, url) :
	def getCharImageURL(br) :
		leader_cardA = br.find_element_by_xpath("//*[@id='leader_card']/a")
		href     = leader_cardA.get_attribute("href")
		imageURL = IMAGE_URL_EXTRACTOR.findall(href)[0]
		return imageURL

	def getCardImageURL(br) :
		cardImage = br.find_element_by_xpath("//*[@id='leader_card']/a/img")
		imgSrc    = cardImage.get_attribute("src")
		return imgSrc


	br.get(url)

	# get card name
	leader_name = br.find_element_by_id('leader_name').text
	leader_name = leader_name.split('\n')[0].strip()
	print("download : " + leader_name)
	
	# get chara image url
	charImageURL       = getCharImageURL(br)
	charImageLocalName = os.path.splitext(os.path.basename(charImageURL))[0] + ".png"
	charImageLocalName = "%s_char_%s" % (leader_name, charImageLocalName)
	
	# get card image url
	cardImageURL       = getCardImageURL(br)
	cardImageLocalName = "%s_card_%s" % (leader_name, os.path.basename(cardImageURL))

	# save
	_downloadImage(br, charImageURL, charImageLocalName)
	_downloadImage(br, cardImageURL, cardImageLocalName)

# @return link list
def _parseCardListPage(br) :
	aList = br.find_elements_by_xpath("//dt/a")
	return [ a.get_attribute('href') for a in aList ]

# 「プレイヤー情報」とかメニューがあるページが表示されてるとする
def _moveToCardListPage(br) :
	cardListA = br.find_element_by_xpath("//a[text()='カード一覧']")
	_wait()
	cardListA.click()

def _login(br) :
	br.get(TOP_PAGE)
	id = br.find_element_by_name('segaid')
	id.send_keys(USERID)
	passwd = br.find_element_by_name('passwd')
	passwd.send_keys(PASSWORD)
	
	_wait()
	submit = br.find_element_by_xpath("//input[@type='submit']")
	submit.click()

def main() :
	global USERID
	global PASSWORD
	
	USERID   = os.environ["PQAC_ID"]   if os.environ["PQAC_ID"]   else USERID
	PASSWORD = os.environ["PQAC_PASS"] if os.environ["PQAC_PASS"] else PASSWORD

	br = launchBrowser()
	br.implicitly_wait(20)
	
	_login(br)
	print("ログインしました")
	_moveToCardListPage(br)
	print("所有カード一覧ページを表示しました")
	cardPageList = _parseCardListPage(br)
	print("所有カード一覧を取得しました // 枚数 : %d" % (len(cardPageList),))
	_wait()
	
	for card in cardPageList :
		try :
			_downloadImageFromDetailPage(br, card)
		except Exception as ex :
			traceback.print_exc()
		_wait()

	br.close()

if __name__ == '__main__' :
	main()
