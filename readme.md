ぷよぷよクエストアーケード カードゲットするやつ
===========================================

なにこれ
-------

ぷよクエACのマイデータサービスから、カードの画像を (σ・∀・)σｹﾞｯﾂ!! します。

つかいかた
---------

ハードルはちょっと高い（わざと）

1. python3 をインストールして良い感じに使える環境にします。詳しくはぐぐれ！
2. コマンドプロンプトで `pip install selenium` して、seleniumを導入します
3. Chrome Driver をパスの通ってる所におきます
4. ID/Passを設定します
5. `python pqacgsy.py` します
6. 時間かかるので、カレー食べながら待ちます
7. ｷｬｯｷｬｳﾌﾌします

### 2. `pip install selenium` して、seleniumを導入します

必要とするモジュールを導入します。

コマンドプロンプトで `pip install selenium` すれば勝手に拾ってきてくれます。

場合によっては「pipのコマンドが古い！2回死ね！」と怒られることがありますが、その際はエラーメッセージ内に表示されるコマンドを実行してアップデートしてください。 `python -m pip install --upgrade pip` みたいな感じのやつ（わすれた）

### 3. Chrome Driver をパスの通ってる所におきます

Python から Chrome を操作するためのプログラムを拾ってきます。

[ChromeDriver - WebDriver for Chrome](https://sites.google.com/a/chromium.org/chromedriver/)  から、"Latest Release: ChromeDriver N.NN" をダウンロードし、いわゆる「Pathの通っている所」に配置します。

### 4. ID/Passを設定します

`pqacgsy.py` をテキストディタで開いて、ユーザーID / パスワードを記述します。 `USERID` に ユーザーID / `PASSWORD` にパスワードを設定してください。


enjoy! :-)
